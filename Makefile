up: vendor
	@docker-compose up

vendor:
	@docker run --rm \
		-v $(CURDIR):/data \
		imega/composer \
		update --ignore-platform-reqs

.DEFAULT_GOAL := up
