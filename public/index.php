<?php

require_once __DIR__ . "/../vendor/autoload.php";

$app = new Silex\Application([
    "debug" => true,
]);

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__. '/../app/views',
));

$app->get("/", function() use ($app) {
    return $app['twig']->render('index.twig');
});


$app->get("{uri}", function($uri) use ($app) {
    return $app['twig']->render(trim($uri, '/'). ".twig", [
        "uri" => $uri,
    ]);
})->assert("uri", ".*");

$app->run();
